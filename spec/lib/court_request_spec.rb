require'spec_helper'

describe CourtRequest do
  let!(:year) { '2000' }
  let!(:number) { '123' }
  let!(:type) { 'P' }

  subject { described_class.new(year, number, type) }

  describe '#build_data' do
    it 'responds_to' do
      expect(subject).to respond_to(:build_data)
    end
    context 'valid request' do
      before do
        @response = subject.build_data
      end
      it 'builds a hash' do
        expect(@response).to be_a(Hash)
      end

      it 'returns the correct data' do
        expect(@response.to_json).to match_response_schema('case_data')
      end
    end
  end

end
