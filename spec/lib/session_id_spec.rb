require'spec_helper'

describe SessionId do
  subject { described_class.new }

  describe '#get_session_id' do
    it 'responds_to' do
      expect(subject).to respond_to(:get_session_id)
    end

    it 'returns a valid token' do
      token = subject.get_session_id
      expect(token).to be_a(String)
    end
  end
end