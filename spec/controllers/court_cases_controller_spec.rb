require 'spec_helper'

describe CourtCasesController, :type => :controller do
  let!(:year) { '1' }
  let!(:number) { '123' }
  let!(:type) { '15' }

  describe '#get_case' do
    context 'valid params' do
      before do
        post :get_case, year: year, number: number, type: type
      end

      it 'renders the correct view' do
        expect(response).to render_template('court_cases/show_case_info')
      end

      it 'responds with html' do
        expect(response.content_type).to eq "text/html"
      end
    end
  end

end