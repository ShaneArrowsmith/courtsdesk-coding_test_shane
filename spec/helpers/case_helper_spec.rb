require 'spec_helper'

describe CaseHelper do
  include CaseHelper

  let!(:year) { '1' }
  let!(:number) { '123' }
  let!(:type) { '15' }

  describe '#year_helper' do
    it 'returns the correct year' do
      expect(year_helper year).to eql '2000'
    end

    it 'returns the correct type' do
      expect(type_helper type)
    end
  end

end