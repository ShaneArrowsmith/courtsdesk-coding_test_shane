module CaseHelper

  def year_helper year_ref
    YEARS[year_ref.to_i - 1][0]
  end

  def type_helper type_ref
    TYPES[type_ref.to_i - 1][0]
  end

  def number_helper number
    number.first
  end
end