class CourtCasesController < ApplicationController
  require 'court_request'
  include CaseHelper

  def get_case
    year = year_helper params[:year]
    type = type_helper params[:type]
    number = number_helper params[:number]
    service = ::CourtRequest.new(year, number, type)
    @case_data = service.build_data
    render 'show_case_info'
  end

end