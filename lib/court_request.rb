require 'nokogiri'
require 'open-uri'
require 'session_id.rb'

class CourtRequest

  def initialize year, number, type
    @year = year
    @number = number
    @type = type
    @session_id = get_session
    @url_base = ENV["COURTS_URL_BASE"]
    @collected_data = {}
  end

  def build_data
    get_case_details
    parse_table_based_pages
    parse_form_based_pages
    @collected_data.as_json
  end

  private

  def get_case_details
    page = get_page 'case_detail'
    @collected_data[:case_name] = page.css("div.case_summary").text
    @collected_data[:case_detail] = parse_form page, DETAIL_KEYS
  end

  def parse_table_based_pages
    TABLES.each do |array|
      page = get_page array[0]
      @collected_data[array[0].to_sym] = parse_table page, array[1]
    end
  end

  def parse_form_based_pages
    FORMS.each do |array|
      page = get_page array[0]
      @collected_data[array[0].to_sym] = parse_form page, array[1]
    end
  end

  def get_session
    session_getter = SessionId.new
    session_getter.get_session_id
  end

  def get_page option
    url = @url_base + option + '.show?sessionID=' + @session_id + '&yearNo=' + @year + '&recordNo=' + @number + '&processType=' + @type
    Nokogiri::HTML(open(url))
  end

  def parse_form page, constant
    hash = {}
    x = 0
    page.css("div.standard_text").each do |data|
      hash[constant[x]] = data.text.delete!("\n").rstrip.lstrip
      x += 1
    end
    hash
  end

  def parse_table page, constant
    array = []
    x = constant.length
    page.xpath('//table/tr').collect do |row|
      hash = {}
      item = []
      0.upto(x) {|x| item.push ([constant[x], "td[#{x+1}]/text()"]) }
      item.each do |key, val|
        hash[key] = row.at_xpath(val).to_s.strip
      end
      array << hash
    end
    check_array array
  end

  def check_array array
    if array.length > 1
      array.shift
      array
    else
      array.first
    end
  end
end

