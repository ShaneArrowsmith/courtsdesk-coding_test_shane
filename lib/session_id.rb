require 'nokogiri'
require 'open-uri'

class SessionId

  def initialize
    @url = ENV["COURTS_SESSION_BASE"]
  end

  def get_session_id
    login_page = Nokogiri::HTML(open(@url))
    login_page.css("input#sessionID")[0]['value']
  end

end
