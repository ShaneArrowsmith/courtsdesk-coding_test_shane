TYPES = [['CA', 1],
         ['CAT', 2],
         ['CCA', 3],
         ['CIR', 4],
         ['CLA', 5],
         ['COS', 6],
         ['EEO', 7],
         ['EXT', 8],
         ['FJ', 9],
         ['FTE', 10],
         ['IA', 11],
         ['JR', 12],
         ['JRP', 13],
         ['MCA', 14],
         ['P', 15],
         ['PAP', 16],
         ['PEP', 17],
         ['PIR', 18],
         ['R', 19],
         ['S', 20],
         ['SA', 21],
         ['SC', 22],
         ['SP', 23],
         ['SS', 24],
         ['SSP', 25],
         ['TBA', 26],
         ['VL', 27]]

YEARS = [['2000', 1],
         ['2001', 2],
         ['2002', 3],
         ['2003', 4],
         ['2004', 5],
         ['2005', 6],
         ['2006', 7],
         ['2007', 8],
         ['2008', 9],
         ['2009', 10],
         ['2010', 11],
         ['2011', 12],
         ['2012', 13],
         ['2013', 14],
         ['2014', 15],
         ['2015', 16],
         ['2016', 17]]

DETAIL_KEYS = [
               :issue_date,
               :setting_down_date,
               :sitting_down_no,
               :appeal_court_ref,
               :appeal_court_ref,
               :date_appeal_court_appeal_lodged,
               :supreme_court_ref,
               :date_supreme_court_appeal_lodged
             ]

PLAINTIFF_KEYS = [:plaintiff_name, :solicitor_name]
DEFENDANT_KEYS = [:defendant_name, :solicitor_name]
JUDGMENT_KEYS = [:judge, :delivered, :distributed]
CASE_FILING_KEYS = [:date, :number, :title, :solicitor]
ORDER_DETAIL_KEYS = [:dated, :result, :perfected, :index_no, :collected, :registrar, :further_info]
COURT_LIST_KEYS = [:date, :list, :position, :result, :note]

TABLES = [['case_filing', CASE_FILING_KEYS],
          ['order_detail', ORDER_DETAIL_KEYS],
          ['court_lists', COURT_LIST_KEYS]]

FORMS = [['plaintiff_detail', PLAINTIFF_KEYS],
         ['defendant_detail', DEFENDANT_KEYS],
         ['judgment_detail', JUDGMENT_KEYS]]