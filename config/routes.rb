Rails.application.routes.draw do

  post '/court_cases/get_case' => 'court_cases#get_case'
  get '/court_cases/show_case_info' => 'court_cases#show_case_info'

  root to: "court_cases#index"
end
